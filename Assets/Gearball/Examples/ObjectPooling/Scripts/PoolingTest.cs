﻿using UnityEngine;
using UnityEngine.UI;
using GearBall.ObjectPooling;
using System.Collections;

public class PoolingTest : MonoBehaviour {

  [SerializeField] int maxObject = 1000;
  [SerializeField] int maxLifetime = 5;
  [SerializeField] Text txtCount;
  [SerializeField] CollisionSphere prefab;
  [SerializeField] Vector3 maxArea;

  int count;
  int active;
  int fps;

  public static PoolingTest Instance;

  void Start () {
    Instance = this;
    StartCoroutine (FPS ());
  }

  void Update () {
    if (active < maxObject) {
      CollisionSphere sphere = ObjectPool.Spawn (prefab, "", null, "Sphere", GetSpawnPos (), Quaternion.identity);
      sphere.lifetime = maxLifetime;
      count++;
      active++;
    }
    fps = Mathf.RoundToInt (1f / Time.deltaTime);
    txtCount.text = string.Format ("Max: {0}, Spawned {1}, Active: {2}, Lifetime: {3}s, FPS: {4}", maxObject, count, active, maxLifetime, fps);
  }

  IEnumerator FPS () {
    while (true) {
      if (Time.timeScale == 1) {
        yield return new WaitForEndOfFrame ();
        fps = Mathf.RoundToInt (1f / Time.deltaTime);
      }
      yield return new WaitForEndOfFrame ();
    }
  }

  public void Recycle (CollisionSphere sphere) {
    ObjectPool.Recycle (sphere);
    active--;
  }

  Vector3 GetSpawnPos () {
    return new Vector3 (Random.Range (-maxArea.x, maxArea.x), Random.Range (-maxArea.y, maxArea.y), Random.Range (-maxArea.z, maxArea.z));
  }

}

