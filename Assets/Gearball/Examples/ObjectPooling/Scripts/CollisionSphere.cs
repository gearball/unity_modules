﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GearBall.ObjectPooling;

public class CollisionSphere : PoolableObject {

  public float lifetime;

  protected override void OnSpawned () {
    base.OnSpawned ();
    GetComponent<Rigidbody> ().velocity = Vector3.zero;
  }

  void Update () {
    if (lifetime > 0) {
      lifetime -= Time.deltaTime;
      if (lifetime <= 0) {
        PoolingTest.Instance.Recycle (this);
      }
    }
  }

	
}
