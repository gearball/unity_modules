﻿

namespace GearBall.ObjectPooling {

  using System.Collections.Generic;
  using UnityEngine;

  /// <summary>
  /// Performance optimizer in creating new game objects
  /// </summary>
  public class ObjectPool : MonoBehaviour {

    #region Private Variables

    /// <summary>
    /// Variable for storing game object.
    /// </summary>
    Dictionary<string,List<IPoolableObject>> objectList;

    /// <summary>
    /// Total number of stored object.
    /// </summary>
    int objectCount;

    /// <summary>
    /// Actual instance of this ObjectPool
    /// </summary>
    static ObjectPool instance;

    /// <summary>
    /// Instance of this ObjectPool
    /// </summary>
    static ObjectPool Instance {
      get
      {
        if(!instance)
        {
          // check if there is a GO instance already available in the scene graph
          instance = FindObjectOfType(typeof(ObjectPool)) as ObjectPool;

          // nope, create a new one
          if(!instance)
          {
            var obj = new GameObject ("ObjectPool");
            instance = obj.AddComponent<ObjectPool> ();
            DontDestroyOnLoad (obj);

            Initialize ();
          }
        }

        return instance;
      }
    }

    #endregion

    #region Private Method

    /// <summary>
    /// Initialize this instance.
    /// </summary>
    static void Initialize () {
      instance.objectCount = 0;
      instance.objectList = new Dictionary<string, List<IPoolableObject>> ();
    }

    #endregion

    #region Public Method

    /// <summary>
    /// Recycles a game object, storing it to the <paramref name="listObject"/> for future use.
    /// </summary>
    /// <param name="ToBeRecycled">Game object to be recycled.</param>
    public static void Recycle<T> (T ToBeRecycled, string id = "") where T : PoolableObject {
      string types = id.Length > 0 ? id : typeof(T).ToString();
      if (!Instance.objectList.ContainsKey(types)) {
        Instance.objectList.Add(types, new List<IPoolableObject>());
      }
      ToBeRecycled.IsSpawned = false;
      ToBeRecycled.name = "";
      ToBeRecycled.transform.SetParent (Instance.transform, false);
      ToBeRecycled.gameObject.SetActive (false);
      Instance.objectList[types].Add (ToBeRecycled);
      Instance.objectCount++;
    }

    /// <summary>
    /// Spawns a game object, instantiate if there are none left.
    /// </summary>
    /// <param name="ToBeSpawned">Game object to be spawned.</param>
    public static T Spawn<T>(T prefab, string id = "", Transform parent = null, string name = "", Vector3 position = new Vector3 (), Quaternion rotation = new Quaternion ()) where T : PoolableObject {
      string types = id.Length > 0 ? id : typeof(T).ToString();
      T output;
      if (!Instance.objectList.ContainsKey(types) || Instance.objectList[types].Count == 0) {
        output = Instantiate<T>(prefab);
      } else {
        output = Instance.objectList[types][Instance.objectList[types].Count - 1] as T;
        Instance.objectList[types].RemoveAt(Instance.objectList[types].Count - 1);
        Instance.objectCount--;
      } 
      output.transform.SetParent (parent);
      if (name.Length > 0) {
        output.gameObject.name = name;
      }
      if (!position.Equals (Vector3.zero)) {
        output.transform.position = position;
      }
      if (!rotation.Equals (Quaternion.identity)) {
        output.transform.rotation = rotation;
      }
      output.gameObject.SetActive (true);
      output.IsSpawned = true;
      return output;
    }

    /// <summary>
    /// Cleans type <paramref name="T"/>
    /// </summary>
    public static void Clean<T>() where T : PoolableObject {
      string types = typeof(T).ToString();
      if (Instance.objectList.ContainsKey (types)) {
        int num = Instance.objectList[types].Count;
        for (int i = 0; i < num; i++) {
          T t = Instance.objectList[types][Instance.objectList[types].Count - 1] as T;
          Instance.objectList[types].RemoveAt(Instance.objectList[types].Count - 1);
          Destroy(t.gameObject);
        }
        Instance.objectCount -= num;
      }
    }

    #endregion

  }

}