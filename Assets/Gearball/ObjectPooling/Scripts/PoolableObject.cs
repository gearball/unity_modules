﻿


namespace GearBall.ObjectPooling {

  using System;
  using UnityEngine;

  /// <summary>
  /// Object that can be stored in an <paramref name="ObjectPool"/>
  /// </summary>
  public class PoolableObject : MonoBehaviour, IPoolableObject {
    bool isSpawned;
    public bool IsSpawned {
      get {
        return isSpawned;
      }
      set {
        isSpawned = value;
        OnSpawned ();
      }
    }

    protected virtual void OnSpawned () {}
  }

}
